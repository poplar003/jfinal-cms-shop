package com.gz.common;

import com.gz.common.model.Xiaochengxu;

public class SystermService {
    private static SystermService service;

    private SystermService() {
    }

    public static SystermService getService() {
        if (service == null) {
            service = new SystermService();
        }
        return service;
    }
    public Xiaochengxu getXiaochengxu(){
        return Xiaochengxu.dao.findFirst("SELECT * FROM tb_xiaochengxu ");
    }
}
